MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('chart', {
      url: "/chart",
      templateUrl: "views/chart.html",
      data: {pageTitle: 'Test Management'},
      controller: "ChartController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([{
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [

              '../assets/global/plugins/highchart/highchart.js',

              'js/factories/ChartFactory.js',
              'js/controllers/ChartController.js'

            ]
          },{
          name: 'MetronicApp',
            files: [
              
              '../assets/global/plugins/highchart/highchart.js',

              'js/factories/ChartFactory.js',
              'js/controllers/ChartController.js'
            ]
          }]);
        }]
      }
    })
}]);
