MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('login', {
      url: "/login",
      templateUrl: "views/login.html",
      data: {pageTitle: 'Test Management'},
      controller: "AuthenticationController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([{
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [

              'js/factories/AuthenticationFactory.js',
              'js/controllers/AuthenticationController.js'

            ]
          },{
          name: 'MetronicApp',
            files: [
              
              'js/factories/AuthenticationFactory.js',
              'js/controllers/AuthenticationController.js'
            ]
          }]);
        }]
      }
    })
}]);
