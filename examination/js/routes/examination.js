MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('exam', {
      url: "/exam",
      templateUrl: "views/exam.html",
      data: {pageTitle: 'Test Management'},
      controller: "ExaminationController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([{
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [

              'js/factories/ScoreFactory.js',
              'js/factories/ExaminationFactory.js',
              'js/controllers/ExaminationController.js'

            ]
          },{
          name: 'MetronicApp',
            files: [
              
              'js/factories/ScoreFactory.js',
              'js/factories/ExaminationFactory.js',
              'js/controllers/ExaminationController.js'
              
            ]
          }]);
        }]
      }
    })

    .state('list', {
      url: "/list",
      templateUrl: "views/list.html",
      data: {pageTitle: 'Test Management'},
      controller: "ExaminationController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([{
            name: 'MetronicApp',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [

              'js/factories/ScoreFactory.js',
              'js/factories/ExaminationFactory.js',
              'js/controllers/ExaminationController.js'

            ]
          },{
          name: 'MetronicApp',
            files: [
              
              'js/factories/ScoreFactory.js',
              'js/factories/ExaminationFactory.js',
              'js/controllers/ExaminationController.js'
              
            ]
          }]);
        }]
      }
    })

}]);
