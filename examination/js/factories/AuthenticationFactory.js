MetronicApp
  .factory('AuthenticationFactory', function($rootScope, $resource) {
    var url = $rootScope.settings.serviceBaseURL;
    var page = 0;
    var size = 20;
    var sort = 'number,desc';

    var member = $resource(url + '/member', {
  
    }, {
    	'create': {
      	method: 'POST'
    	}
  	});

    return member;
    
  });
