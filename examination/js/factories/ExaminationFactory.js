MetronicApp
  .factory('ExaminationFactory', function($rootScope, $resource) {
    var url = $rootScope.settings.serviceBaseURL;
    var page = 0;
    var size = 20;
    var sort = 'number,desc';

    var exam = $resource(url + '/exam', {
  
    }, {});

    exam.list = function() {
    	return $resource(url + '/list', {
  
    	}, {});
    }

    return exam;
  });
