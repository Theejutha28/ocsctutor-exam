MetronicApp
  .factory('ChartFactory', function($rootScope, $resource) {
    var url = $rootScope.settings.serviceBaseURL;
    var page = 0;
    var size = 20;
    var sort = 'number,desc';

    var chart = $resource(url + '/chart', {
  
    }, {});
    return chart;
  });