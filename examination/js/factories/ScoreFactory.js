MetronicApp
  .factory('ScoreFactory', function($rootScope, $resource) {
    var url = $rootScope.settings.serviceBaseURL;
    var page = 0;
    var size = 20;
    var sort = 'number,desc';

    var score = $resource(url + '/score', {
  
    }, {
      'create': {
        method: 'POST'
      },
      'update': {
        method: 'PUT'
      },
      'delete': {
        method: 'DELETE'
      }
  	});
    return score;
  });
