angular.module('MetronicApp').controller('ExaminationController',
  function($rootScope, $scope, $templateCache, $state, $stateParams, $filter, $location, $http,
  $uibModal, $compile, $localStorage, settings, ExaminationFactory) {

  	$scope.$on('$viewContentLoaded', function() {
   		// initialize core components

    	App.initAjax();
  	});
  	$rootScope.settings.layout.pageContentWhite = true;
  	$rootScope.settings.layout.pageBodySolid = false;
  	$rootScope.settings.layout.pageSidebarClosed = false;

  	$scope.choice = {};
    $scope.histories = [];
  	$scope.answer = "";
  	$scope.current_place = {};
  	$scope.disabled_button_next = true;
  	$scope.disabled_radio_choice = false;
  	$scope.show_button_next = true;
  	$scope.show_button_score = false;	
  	$scope.scores = 0;

    ExaminationFactory.list().get({

    }, function(result){
      $scope.lists = result.data;
    }, function(error) {
      return $scope.lists = [];
    });
    
    ExaminationFactory.get({

    }, function(result){
      $scope.exams = result.data;
      var i = 1;

      if(i<= $scope.exams.length) {
        $scope.current_place = $scope.exams[i-1];
      }
    }, function(error) {
      return $scope.exams = [];
    });

    $scope.check = function(choice,id) {

    	if (choice == $scope.exams[id-1].answer.choices_id) {

    		$scope.answer = "ถูกต้อง";
    		$scope.scores++;
    		$scope.disabled_radio_choice = true;
    		$scope.disabled_button_next = false;
    		$scope.success = $scope.exams[id-1].answer.choices_id;

    	} else {

    		$scope.answer = "ผิด";
    		$scope.disabled_radio_choice = true;
    		$scope.disabled_button_next = false;
    		$scope.success = $scope.exams[id-1].answer.choices_id;

    	}

      $scope.histories.push(
        $filter('filter')($scope.exams, {id: choice.id}, true)[0]
      );
    	
    };

    $scope.test = function() {
    	$scope.scores++;
    };

    $scope.next = function(answer,id) {

  		$scope.choice = {};
  		$scope.answer = "";
  		$scope.disabled_button_next = true;
  		$scope.disabled_radio_choice = false;
  		$scope.success = {};

  		var i = 1+id;
      
  		if(i < $scope.exams.length) {
    		$scope.current_place = $scope.exams[id];
    	} else if (i == $scope.exams.length) {
    		$scope.current_place = $scope.exams[id];
    		$scope.show_button_next = false;
			  $scope.show_button_score = true;
    	}

  	};

  	$scope.score = function() {
  		var modalInstance = $uibModal.open({
        templateUrl: 'modal_score.html',
        controller: 'ModalScoreController',
        windowClass: 'large-modal',
        backdrop: 'static',
        resolve: {
          scores: function() {
            return $scope.scores;
          },
          histories: function() {
            return $scope.histories;
          }
        }
      });

      modalInstance.result.then(function(result) {
        $state.reload();
      }).catch(function(result) {
        // $scope.active = result;
        console.log('Modal dismissed at:' + new Date());
      });
  	};

    
  }).controller('ModalScoreController', function($scope, $uibModalInstance, $state, ScoreFactory,
  scores, histories) {
    //$scope.score = JSON.stringify(scores);
    $scope.score = scores;
    $scope.histories = histories

    $scope.yes = function() {
      console.log($scope.histories);
      console.log($scope.score);

      ScoreFactory.create({
      }, {
        data: $scope.score
      }, function(result) {
        console.log(result);
        $uibModalInstance.close();
      });
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };

  });

