angular.module('MetronicApp').controller('ChartController',
  function($rootScope, $scope, $templateCache, $state, $stateParams, $filter, $location, $http,
  $uibModal, $compile, $localStorage, settings, ChartFactory) {

  	$scope.$on('$viewContentLoaded', function() {
   		// initialize core components

    	App.initAjax();
  	});
  	$rootScope.settings.layout.pageContentWhite = true;
  	$rootScope.settings.layout.pageBodySolid = false;
  	$rootScope.settings.layout.pageSidebarClosed = false;   

    ChartFactory.get({

    }, function(response) {

      $scope.chartScore(response.data);

    });

    Highcharts.chart('container', {
      chart: {
        renderTo: 'container',
        type: 'line'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: ['ครั้งที่ 1', 'ครั้งที่ 2', 'ครั้งที่ 3', 'ครั้งที่ 4', 'ครั้งที่ 5'
        ]
      },
      yAxis: {
        title: {
          text: 'คะแนน'
        }
      },

      series: [{
        id: 1,
        name: 'การสะกดคำ',
        data: [29.9, 71.5, 66.4, 129.2, 144.0],
        color: '#6666ff'
      },{
        id: 2,
        name: 'พยัญชนะ',
        data: [76.0, 135.6, 148.5, 180.4, 154.1],
        color: 'red'
      },{
        id: 3,
        name: 'สระ',
        data: [16.0, 35.6, 48.5, 116.4, 194.1],
        color: '#90ed7d'
      }]
    });

    $scope.chartScore = function(data) {
      Highcharts.chart('pie', {
        chart: {
          renderTo: 'container',
          type: 'pie'
        },
        title: {
          text: ''
        },

        plotOptions: {
          series: {
            dataLabels: {
              enabled: true,
              format: '{point.name}: {point.y:.1f}%',
              style: { fontSize: '18px' }
            }
          }
        },

        "series": [
          {
            "name": "Scores",
            "colorByPoint": true,
            "data": data
          }
        ]

      });
    };

  });

